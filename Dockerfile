FROM debian:stable AS builder

RUN apt-get update
RUN apt-get -y install build-essential git libssl-dev

# Musl
RUN git clone --branch v1.1.24 --depth 1 git://git.musl-libc.org/musl
RUN cd musl && ./configure --prefix=/opt/musl --exec-prefix=/opt --syslibdir=/opt/lib --disable-shared && make && make install

# Kernel headers
RUN git clone --depth 1 https://github.com/sabotage-linux/kernel-headers
RUN cd kernel-headers && make ARCH=x86_64 prefix=/opt/musl install && cd -

# ToyBox
RUN git clone --depth 1 https://github.com/landley/toybox
RUN grep -r CC toybox

WORKDIR toybox

RUN make V=1 CC="/opt/bin/musl-gcc" defconfig
RUN sed -i -e "s/# CONFIG_SH is not set/CONFIG_SH=y/" .config
RUN make V=1 CC="/opt/bin/musl-gcc" CFLAGS="-O3" LDFLAGS="-O3 --static"

FROM scratch

WORKDIR /

COPY --from=builder toybox/toybox .

ENTRYPOINT ["/toybox"]
