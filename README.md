
## TODO

### musl

```
CC="musl-gcc -static" ./configure --prefix=$HOME/musl && make
```

*NOTE* build musl with --disabled-shared to reduce build times

## Building in the bootstrap environment

```
docker build -t microlinux-build:1 bootstrap
BUILD_ID=$(docker create -it microlinux-build:1)
docker cp ${BUILD_ID}:toybox/toybox .
docker rm ${BUILD_ID}
```

## Debugging tips

```
docker run -it --rm microlinux-build:1
```

## Bugs

need to fix up the ELF interpreter...

-Wl,-dynamic-linker,/my/lib/ld-linux.so.2

