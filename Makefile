MINILINUX_VERSION=0.1.0
CONTAINER_IMAGE=minilinux:${MINILINUX_VERSION}
DOCKER=docker
.ONESHELL :
.PHONY : all clean clean-all
all :: toybox
clean ::
	$(RM) toybox
clean-all :: clean
	${DOCKER} rmi ${CONTAINER_IMAGE} || true
toybox : Dockerfile
	set -e
	${DOCKER} build -t ${CONTAINER_IMAGE} .
	BUILD_ID=$$(${DOCKER} create -it ${CONTAINER_IMAGE})
	echo BUILD_ID=$${BUILD_ID}
	${DOCKER} cp $${BUILD_ID}:/toybox .
	${DOCKER} rm $${BUILD_ID}
